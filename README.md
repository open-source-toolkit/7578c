# 基于C#的Modbus TCP类库

---

## 概述

本仓库提供了一个高效、易于集成的C# Modbus TCP类库，专为需要通过TCP/IP协议进行设备通信的应用程序设计。Modbus是一种广泛应用于工业自动化领域的通信协议，支持多种数据类型和不同的物理层，而此库专注于TCP实现，简化了在C#项目中实施Modbus通信的过程。

## 特性

- **简单易用**：封装了复杂的TCP通信细节，使得开发者能够快速地在应用中集成Modbus TCP通信功能。
- **高度模块化**：类库设计考虑到了可扩展性和重用性，便于根据不同需求定制功能。
- **稳定可靠**：实现了错误处理机制，增强了网络通信的健壮性。
- **兼容标准**：遵循Modbus协议标准，确保与各种支持Modbus TCP的设备兼容。
- **示例代码**：包含简单的使用示例，帮助开发者快速上手。

## 快速入门

1. **依赖引入**：将本仓库的DLL文件添加到您的C#项目中，或直接通过NuGet包管理器（如果已发布为NuGet包）来安装。
2. **初始化连接**：创建`ModbusTcpClient`实例，并设置正确的IP地址和端口号。
3. **读写寄存器**：使用提供的方法（如`ReadHoldingRegisters`和`WriteSingleRegister`）来与设备交互。
4. **异常处理**：适当处理可能发生的网络或协议相关的异常。

```csharp
using YourNamespace; // 假设类库命名空间是YourNamespace

// 初始化客户端
ModbusTcpClient client = new ModbusTcpClient("192.168.1.100", 502);

try
{
    // 连接设备
    client.Connect();

    // 例如，读取保持寄存器
    ushort[] registers = client.ReadHoldingRegisters(1, 10); // 从地址1开始读取10个寄存器

    // 写入单个寄存器的示例
    client.WriteSingleRegister(1, (ushort)42);
}
catch (Exception ex)
{
    Console.WriteLine($"Error: {ex.Message}");
}
finally
{
    // 断开连接
    if (client.IsConnected)
        client.Disconnect();
}
```

## 注意事项

- 确保目标设备支持Modbus TCP协议，并正确配置了相应的地址和参数。
- 在生产环境中，请彻底测试以确保稳定运行。
- 考虑到安全性和性能，推荐在频繁操作时合理控制连接和断开的频率。

## 贡献指南

欢迎贡献代码、提出问题或建议。请遵守项目的贡献准则，在提交任何更改前先讨论，确保变化与项目方向一致。

## 许可证

该项目采用了[MIT许可证](LICENSE)，允许免费用于商业和个人项目。

---

加入我们，一起探索和优化工业通信的世界，让设备间的对话更加顺畅无阻。